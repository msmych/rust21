use add_one;
use add_two;

fn main() {
    let num = 10;
    println!("Hello, word! {} plus one is {}!", num, add_one::add_one(num));
    println!("And 2 plus 2 is {}", add_two::add_two(2));
}
