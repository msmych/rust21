use std::rc::Rc;
use std::cell::RefCell;
use List::{Cons, Nil};

#[derive(Debug)]
enum List {
    Cons(Rc<RefCell<i32>>, Rc<List>),
    Nil,
}

fn main() {
    let list = Cons(Rc::new(RefCell::new(1)), 
        Rc::new(Cons(Rc::new(RefCell::new(2)), 
            Rc::new(Cons(Rc::new(RefCell::new(3)), 
                Rc::new(Nil))))));
    let value = Rc::new(RefCell::new(5));
    let a = Rc::new(Cons(Rc::clone(&value), Rc::new(Cons(Rc::new(RefCell::new(10)), Rc::new(Nil)))));
    println!("count after creating a = {}", Rc::strong_count(&a));
    let b = Cons(Rc::new(RefCell::new(3)), Rc::clone(&a));
    println!("count after creating b = {}", Rc::strong_count(&a));
    *value.borrow_mut() += 10;
    {
        let c = Cons(Rc::new(RefCell::new(4)), Rc::clone(&a));
        println!("count after creating c = {}", Rc::strong_count(&a));
        println!("c after = {:?}", c);
    }
    println!("count after c goes out of scope = {}", Rc::strong_count(&a));
    println!("a after = {:?}", a);
    println!("b after = {:?}", b);
}
